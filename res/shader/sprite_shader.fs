#version 330 core

out vec4 color;

in vec4 fs_color;
in vec2 fs_uv;
in float fs_layer;

uniform sampler2DArray texture_sampler;

void main()
{
	color = texture(texture_sampler, vec3(fs_uv.x, fs_uv.y, fs_layer)) * fs_color;
}