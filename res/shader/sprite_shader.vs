#version 330 core

layout(location = 0) in vec2 size;
layout(location = 1) in vec2 position;
layout(location = 2) in vec2 scale;
layout(location = 3) in vec4 color;
layout(location = 4) in float rotation;
layout(location = 5) in float layer;

out vec2 gs_size;
out vec2 gs_pos;
out vec2 gs_scale;
out vec4 gs_color;
out float gs_rotation;
out float gs_layer;

//uniform mat4 proj_matrix;
//uniform mat4 view_matrix;

void main()
{
	gl_Position = vec4(0.0, 0.0, 0.0, 1.0);
	
	//gl_Position = proj_matrix * view_matrix * vec4(position.x, position.y, 1.0, 1.0);
	//gl_PointSize = 10;
	
	gs_size = size;
	gs_pos = position;
	gs_scale = scale;
	gs_color = color;
	gs_rotation = rotation;
	gs_layer = layer;
}
