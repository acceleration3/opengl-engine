#version 330
 
layout(points) in;
layout(triangle_strip, max_vertices = 4) out;
 
in vec2 gs_size[];
in vec2 gs_pos[];
in vec2 gs_scale[];
in vec4 gs_color[];
in float gs_rotation[];
in float gs_layer[];

out vec4 fs_color;
out vec2 fs_uv;
out float fs_layer;

uniform mat4 proj_matrix;
uniform mat4 view_matrix;
uniform vec2 texture_size;

void main()
{
	mat4 translate_matrix = mat4(
		vec4(1, 0, 0, 0),
		vec4(0, 1, 0, 0),
		vec4(0, 0, 1, 0),
		vec4(gs_pos[0].x, gs_pos[0].y, 0, 1)
	);

	mat4 rotate_matrix = mat4(
		vec4(cos(gs_rotation[0]), -sin(gs_rotation[0]), 0, 0),
		vec4(sin(gs_rotation[0]), cos(gs_rotation[0]), 0, 0),
		vec4(0, 0, 1, 0),
		vec4(0, 0, 0, 1)
	);

	mat4 scale_matrix = mat4(
		vec4(gs_scale[0].x, 0, 0, 0),
		vec4(0, gs_scale[0].y, 0, 0),
		vec4(0, 0, 1, 0),
		vec4(0, 0, 0, 1)
	);
	
	mat4 model_matrix = translate_matrix * rotate_matrix * scale_matrix;
	mat4 mvp = proj_matrix * view_matrix * model_matrix;

	gl_Position = mvp * vec4(-gs_size[0].x / 2, -gs_size[0].y / 2, 0, 1);
	fs_uv = vec2(0, 0);
	fs_color = gs_color[0];
	fs_layer = gs_layer[0];
	EmitVertex();
	
	gl_Position = mvp * vec4(-gs_size[0].x / 2, gs_size[0].y / 2, 0, 1);
	fs_uv = vec2(0, gs_size[0].y / texture_size.y);
	fs_color = gs_color[0];
	fs_layer = gs_layer[0];
	EmitVertex();
	
	gl_Position = mvp * vec4(gs_size[0].x / 2, -gs_size[0].y / 2, 0, 1);
	fs_uv = vec2(gs_size[0].x / texture_size.x, 0);
	fs_color = gs_color[0];
	fs_layer = gs_layer[0];
	EmitVertex();
	
	gl_Position = mvp * vec4(gs_size[0].x / 2, gs_size[0].y / 2, 0, 1);
	fs_uv = vec2(gs_size[0].x / texture_size.x, gs_size[0].y / texture_size.y);
	fs_color = gs_color[0];
	fs_layer = gs_layer[0];
	EmitVertex();
	
	EndPrimitive();
}